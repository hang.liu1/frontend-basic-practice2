import { fetchData } from './util/fetchData';
import { createPerson } from './util/createPerson';
import { renderHeader } from './render/header';
import { renderAboutMe } from './render/aboutMe';
import { renderEducations } from './render/educations';

fetchData(URL)
  .then(result => {
    const person = createPerson(result);
    const { name, age, description, educations } = person;
    renderHeader(name, age);
    renderAboutMe(description);
    renderEducations(educations);
  })
  .catch(error => {
    // eslint-disable-next-line no-console
    console.error(error);
  });

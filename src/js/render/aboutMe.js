import $ from 'jquery';

export function renderAboutMe(description) {
  $('#description').text(description);
}

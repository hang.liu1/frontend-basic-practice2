import $ from 'jquery';

export function renderEducations(educations) {
  let ul = $('#educations ul');
  let li_example = $('li:first-child', ul);
  educations.forEach(education => {
    let li_clone = li_example.clone();
    $('h4', li_clone).text(education.year);
    $('div h4', li_clone).text(education.title);
    $('div p', li_clone).text(education.description);

    ul.append(li_clone);
  });
  li_example.hide();
}

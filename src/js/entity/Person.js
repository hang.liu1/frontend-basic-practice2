import { Education } from './Education';

export class Person {
  constructor(name, age, description, educations) {
    this._name = name;
    this._age = age;
    this._description = description;
    this._educations = educations.map(
      education =>
        new Education(education.year, education.title, education.description)
    );
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }

  get educations() {
    return this._educations;
  }
}

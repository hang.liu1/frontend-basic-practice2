import { Person } from '../entity/Person';

export function createPerson(result) {
  return new Person(
    result.name,
    result.age,
    result.description,
    result.educations
  );
}
